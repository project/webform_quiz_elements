<?php

namespace Drupal\webform_quiz_elements\Element;

use Drupal\Core\Render\Element\RenderElementBase;

/**
 * Provides a render element for quiz_element_result.
 *
 * @FormElement("webform_quiz_elements_result")
 */
class WebformQuizElementResult extends RenderElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#quiz_title' => '',
      '#quiz_options' => [],
      '#theme' => 'webform_quiz_elements_result',
      '#attributes' => [],
      '#attached' => [
        'library' => [
          'webform_quiz_elements/styles',
        ],
      ],
    ];
  }

}
